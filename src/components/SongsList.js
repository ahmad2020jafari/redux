import React, { Component } from "react";
import { connect } from "react-redux";
import { selectedSong } from "../actions";

class SongsList extends Component {
  renderList = () => {
    return this.props.songs.map(song => {
      return (
        <div key={song.title} className="list-group">
          <div className="list-group-item my-2">
            <button
              className="btn btn-primary float-right px-3"
              onClick={() => {
                this.props.selectedSong(song);
              }}
            >
              Select
            </button>
            <div className="float-left">{song.title}</div>
          </div>
        </div>
      );
    });
  };

  render() {
    return <div> {this.renderList()} </div>;
  }
}
const mapStateToProps = state => {
  return { songs: state.songs };
};

export default connect(mapStateToProps, { selectedSong })(SongsList);
