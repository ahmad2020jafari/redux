import React from "react";
import { connect } from "react-redux";

const SongsDetail = ({ song }) => {
  if (!song) {
    return <div>Select a Song</div>;
  }
  return (
    <div>
      <h1>Songs</h1>
      <div>
        {song.title} <br />
        {song.duration}
      </div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    song: state.selectedSong
  };
};

export default connect(mapStateToProps)(SongsDetail);
