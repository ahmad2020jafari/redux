import React from "react";
import SongsList from "./SongsList";
import SongsDetail from "./SongsDetail";

const App = () => {
  return (
    <div className="container float-left mt-3">
      <div className="row">
        <div className="col-8">
          <SongsList />
        </div>
        <div className="col-4">
          <SongsDetail />
        </div>
      </div>
    </div>
  );
};

export default App;
