import { combineReducers } from "redux";

const songsReducers = () => {
  return [
    { title: "No Scrups", duration: "4:05" },
    { title: "Marcarena", duration: "2:30" },
    { title: "All Star", duration: "3:15" },
    { title: "I want it, that way", duration: "1:45" }
  ];
};

const selectedSongReducers = (selectedSong = null, action) => {
  if (action.type === "SELECTED_SONG") {
    return action.payload;
  }

  return selectedSong;
};

export default combineReducers({
  songs: songsReducers,
  selectedSong: selectedSongReducers
});
